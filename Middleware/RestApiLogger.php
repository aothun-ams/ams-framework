<?php

namespace Spiral\Middleware;

use Litpi\Helper;
use Litpi\Registry;
use Model\BaseModel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Monolog\Logger;
use Teamcrop\Rest\Base;

class RestApiLogger
{
    /**
     * Call to log request
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        //Get chain identifier from headers
        if ($request->hasHeader('Chain-Identifier')) {
            Base::$requestChainIdentifier = $request->getHeaderLine('Chain-Identifier');
        }

        //Continue with execution

        /** @var ResponseInterface $response */
        $response = $next($request, $response);

        // Get usaged memory (not allocated) until this point
        // In Megabype (MB) unit
        $memoryUsageInMegabyte = round(memory_get_usage() / 1024 / 1024, 4);

        // After response
        $registry = Registry::getInstance();

        $controller = $registry->get('controller');
        $action = $registry->get('action');

        //Get execution time
        $executionTime = round(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"], 3) * 1000;

        //do not tracking options request
        $isAccessTrusted = (int)$registry->get('accesstrusted');

        if ($request->getMethod() != 'OPTIONS' && $action != 'ping' && !$isAccessTrusted) {

            $isAjax = false;
            if ($request->hasHeader('X-Requested-With')
                && $request->getHeader('X-Requested-With')[0] == 'XMLHttpRequest') {
                $isAjax = true;
            }


            $formData = array(
                'a' => $request->getMethod(),
                'b' => $controller,
                'c' => $action ? $action : 'index',
                'd' => (int)$isAjax,
                'e' => (int)$registry->get('company')->id,
                'f' => (int)$registry->get('me')->id,
                'g' => $response->getStatusCode(),
                'h' => $executionTime,
                'i' => $memoryUsageInMegabyte,
                'j' => Helper::getIpAddress(),
                'k' => substr_count(Base::$requestChainIdentifier, ',')
            );


            /** @var Logger $logger */
            $logger = $registry->get('logger');

            // Insert Log information
            $output = '';
            foreach ($formData as $k => $v) {
                $output .= $k . '=' . $v . ',';
            }

            $logger->addNotice($output);
        }

        //Set server addr to debug which server response to this request incase many server of same backend
        if ($action == 'ping' && isset($_SERVER['SERVER_ADDR'])) {
            $response = $response->withHeader('Execution-Server', $_SERVER['SERVER_ADDR']);
        }

        $response = $response->withHeader('Execution-Time', $executionTime . ' ms');
        return $response;
    }
}