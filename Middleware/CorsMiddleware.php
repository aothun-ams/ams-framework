<?php

namespace Spiral\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class CorsMiddleware
{
    /**
     * Call to return headers support for CORS request
     *
     * @param  ServerRequestInterface $request  PSR7 request
     * @param  ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {
        // Get reference to application
        /** @var ResponseInterface $response */
        $response = $response->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS')
            ->withHeader('Access-Control-Allow-Headers', 'Authorization, Content-Type');

        if(strtoupper($request->getMethod()) == 'OPTIONS') {
            //Cache OPTIONS
            $response = $response->withHeader('Access-Control-Max-Age', 1728000);
        } else {
            // Run inner middleware and application
            $response = $next($request, $response);
        }

        return $response;

    }
}