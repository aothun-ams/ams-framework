<?php

namespace Controller;

use Litpi\Registry;
use Litpi\Helper;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Teamcrop\Utility\CompanyModule;
use Elasticsearch\ClientBuilder;

abstract class BaseController
{
    protected $registry;

    /** @var App $app */
    protected $app;

    /** @var ServerRequestInterface $request */
    protected $request;

    /** @var ResponseInterface $response */
    protected $response;

    /** @var \Elasticsearch\Client $searchEngineClient  */
    private $searchEngineClient = null;

    public $queryData;

    public $requestData;

    public $uid = 0;
    public $cid = 0;
    public $accesszone = '';
    public $accesstrusted = false;

    public function __construct(Registry $registry, App $app)
    {
        $this->registry = $registry;
        $this->app = $app;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param string $moduleToCheck. Module need to check has format:
     * (old format: eg: "office.1", "store.1", "store.3"), new format just name
     * @param string $roleToCheck
     * @param array $roleObjectIdList
     * @return bool
     */
    public function init(
        ServerRequestInterface $request,
        ResponseInterface $response,
        $moduleToCheck = '',
        $roleToCheck = '',
        &$roleObjectIdList = array()
    ) {
        $this->request = $request;
        $this->response = $response;
        $this->requestData = $request->getParsedBody();
        $this->queryData = $request->getQueryParams();

        $pass = true;

        if ($moduleToCheck != '' && !$this->hasModule($moduleToCheck)) {
            $pass = false;

            //global assign notpermission if not pass license or role
            $this->renderJson(array(
                'error' => array('error_module_not_available'),
                'detail' => array(
                    'module' => $moduleToCheck
                )
            ), 403);
        }

        if ($roleToCheck != '' && !$this->hasRole($roleToCheck, $roleObjectIdList)) {
            $pass = false;

            //global assign notpermission if not pass license or role
            $this->renderJson(array(
                'error' => array('error_role_not_available'),
                'detail' => array(
                    'role' => $roleToCheck
                )
            ), 403);
        }

        return $pass;
    }

    abstract public function run();

    protected function renderJson($jsonData, $status = 200)
    {
        $this->response = $this->response->withStatus($status);
        $this->response = $this->response->withHeader('Content-Type', 'application/json');
        $this->response->getBody()->write(json_encode($jsonData));
    }

    protected function renderJsDataObject($cacheName, $items)
    {
        $output = 'define(["cacheddata",], function(CachedData){
    return {
        init: function(){
            var data = TAFFY('.json_encode($items).');

            CachedData.set("'.$cacheName.'", data);
        }
    }
});';

        $this->response = $this->response->withStatus(200);
        $this->response = $this->response->withHeader('Content-Type', 'application/javascript');
        $this->response = $this->response->withHeader('Cache-control', 'max-age='.(60*60*24*365) . ',public');
        $this->response = $this->response->withHeader('Expires', gmdate('D, d M Y H:i:s',time()+60*60*24*365) . ' GMT');
        $this->response->getBody()->write($output);

    }

    /**
     * Used to render error with status for all system error
     * @param int $status
     * @param mixed (string | array) $error
     */
    protected function renderError($error, $status = 404)
    {
        $errorList = array();

        if (is_array($error)) {
            $errorList = $error;
        } else {
            $errorList[] = $error;
        }

        $this->renderJson(array('error' => $errorList), $status);
    }

    protected function notfound($status = 404)
    {
        $this->renderError('error_not_found', $status);
    }

    protected function notfound422()
    {
        $this->notfound(422);
    }

    protected function notpermission422()
    {
        $this->renderError('error_not_permission', 422);
    }

    protected function checkScopeCompanyId($cid)
    {
        $pass = false;

        if ($this->hasAccessTrusted() || $cid == $this->registry->company->id) {
            $pass = true;
        }

        return $pass;
    }

    protected function checkScopeUserId($uid)
    {
        $pass = false;

        if ($this->hasAccessTrusted() || $uid == $this->registry->me->id) {
            $pass = true;
        }

        return $pass;
    }

    /**
     * Get current page number in query (get, post, put)
     * It will be used in many place in GET method
     * @param string $pageParamName
     * @return int
     */
    protected function getCurrentPage($pageParamName = 'page')
    {
        $queryData = $this->request->getQueryParams();

        if (!isset($queryData[$pageParamName]) || $queryData[$pageParamName] < 1) {
            $currentPageNumber = 1;
        } else {
            $currentPageNumber = $queryData[$pageParamName];
        }

        return (int)$currentPageNumber;
    }

    /**
     * Get record per page number, used for almost all GET request to query database to fetch data
     * @param int $hardLimit the limit of record per page,
     * if the argument is greater than this value, it will use this value as record per page
     * @param string $limitParamName
     * @return int
     */
    protected function getRecordPerPage($hardLimit = 50, $limitParamName = 'limit')
    {
        $queryData = $this->request->getQueryParams();

        if (!isset($queryData[$limitParamName]) || $queryData[$limitParamName] > $hardLimit) {
            $recordPerPage = $hardLimit;
        } else {
            $recordPerPage = $queryData[$limitParamName];
        }

        return (int)$recordPerPage;
    }

    public function getPaging(
        $defaultSortby = 'id',
        $defaultSorttype = 'DESC',
        $pageParamName = 'page',
        $hardLimit = 50,
        $limitParamName = 'limit'
    ) {
        $queryData = $this->queryData;

        $sortby = $queryData['sort_by'] ? $queryData['sort_by'] : $defaultSortby;
        $sorttype = $queryData['sort_type'] ? $queryData['sort_type'] : $defaultSorttype;

        $page = $this->getCurrentPage($pageParamName);
        $limit = $this->getRecordPerPage($hardLimit, $limitParamName);

        return array($sortby, $sorttype, $page, $limit);
    }

    /**
     * Build query limit string (offset) base on current page and record perpage.
     * @param $currentPage
     * @param $recordPerPage
     * @return string
     */
    protected function getQueryLimit($currentPage, $recordPerPage)
    {
        return ($currentPage - 1 ) * $recordPerPage . ', ' . $recordPerPage;
    }

    /**
     * Extract array data from requestData to create new formData array with prefix (default is 'f').
     * @param string $prefix
     * @return array
     */
    public function extractFormData($prefix = 'f')
    {
        $formData = array();

        foreach ($this->requestData as $k => $v) {
            $formData[$prefix . $k] = $v;
        }

        return $formData;
    }


    /**
     * Use to check valid cid and uid in most of addValidator() of all controllers
     * @param $cid
     * @param $uid
     * @param $error
     * @return bool
     */
    public function validateCompanyUser($cid, $uid, &$error)
    {
        $pass = true;

        // Check valid company
        if ($cid == 0) {
            $error[] = 'error_company_id_required';
            $pass = false;

        } elseif (!$this->checkScopeCompanyId($cid)) {
            $error[] = 'error_company_id_notfound';
            $pass = false;
        }

        // Check valid user
        if ($uid == 0) {
            $error[] = 'error_user_id_required';
            $pass = false;

        } elseif (!$this->checkScopeUserId($uid)) {
            $error[] = 'error_user_id_notfound';
            $pass = false;
        }

        return $pass;
    }

    /**
     * Used for refined CID from request for most of request
     * @param $key
     * @return int
     */
    protected function getCurrentCompanyIdFromRequest($key = 'company_id')
    {
        $method = strtoupper($this->request->getMethod());

        if ($method == 'GET') {
            $queryData = $this->queryData;
        } else {
            //for put, post, delete...
            $queryData = $this->requestData;
        }


        if (isset($queryData[$key])) {
            $cidFromRequest = (int)$queryData[$key];
        } else {
            $cidFromRequest = 0;
        }

        //If does not have admin or accesstrusted
        //filter the cid
        if ($this->hasAccessTrusted()) {
            //Whatever passing cid, just use it ^^! event zero
            $cid = $cidFromRequest;

        } else {
            if ($this->registry->company->id > 0 && $this->registry->company->id == $cidFromRequest) {
                $cid = $cidFromRequest;
            } else {
                //set -1 to invalid company filter
                // Instead of -1,
                // i set to 4.200.000.000 to make cid > 0 (to validate in most of model and it will not found anything)
                // Because we can not create 4200000000 companies :)
                $cid = 4200000000;
            }
        }

        return $cid;
    }

    /**
     * Used for refined UID from request for most of request
     * @param $key
     * @return int
     */
    protected function getCurrentUserIdFromRequest($key)
    {
        $queryData = $this->request->getQueryParams();

        if (isset($queryData[$key])) {
            $uidFromRequest = (int)$queryData[$key];
        } else {
            $uidFromRequest = 0;
        }


        //If does not have admin or accesstrusted
        //filter the uid
        if ($this->hasAccessTrusted()) {
            //Whatever passing uid, just use it ^^! event zero
            $uid = $uidFromRequest;


        } else {
            if ($this->registry->me->id > 0 && $this->registry->me->id == $uidFromRequest) {
                $uid = $uidFromRequest;
            } else {
                // Instead of -1,
                // i set to 4.200.000.000 to make uid > 0 (to validate in most of model and it will not found anything)
                // Because we can not create 4200000000 users :)
                $uid = 4200000000;
            }
        }

        return $uid;
    }

    /**
     * Make plaintext for input array
     * @param array $fieldData
     * @param array $excludeFields
     * @return array $refinedData
     */
    public function plaintext($fieldData, $excludeFields = array())
    {
        $refinedData = array();

        if (is_array($fieldData)) {
            foreach ($fieldData as $k => $v) {
                if (!in_array($k, $excludeFields)) {
                    $refinedData[$k] = trim(Helper::plaintext($v));
                } else {
                    $refinedData[$k] = trim($v);
                }
            }
        } else {
            $refinedData = $fieldData;
        }

        return $refinedData;
    }

    /**
     * Check if current user has this module or not
     * @param string $module. Module need to check
     * @return bool
     */
    public function hasModule($module)
    {
        $pass = false;

        if ($this->hasAccessTrusted()) {
            $pass = true;

        } else {
            //for backward compatible, currently if module has '.' character (it mean store.1, store.3, office.1)
            if (strpos($module, '.') !== false) {
                $pass = true;
            } elseif (is_array($this->registry->company->moduleenable)) {
                $moduleId = CompanyModule::textToId($module);

                //convert moduletext to moduleid
                $pass = $moduleId > 0 && in_array($moduleId, $this->registry->company->moduleenable);
            }
        }

        return $pass;
    }

    /**
     * Check if current user has this role in current company or not
     * @param string $role
     * @param array $objectIdList If this role have limit by object id, this variable will be assign object id list
     * @return bool
     */
    public function hasRole($role, &$objectIdList = array())
    {
        $pass = false;

        if ($this->hasAccessTrusted()) {
            $pass = true;

        } else {
            if (is_array($this->registry->role)) {
                $pass = array_key_exists($role, $this->registry->role);
                if ($pass) {
                    $objectIdList = $this->registry->role[$role];
                }
            }
        }

        return $pass;
    }

    public function hasAccessTrusted()
    {
        return $this->registry->accesstrusted;
    }

    /**
     * Assign formData value (for searching) base on query field
     *
     * @param $formData
     * @param mixed (string | array) $queryDataKey
     * @param array $apiMapFields Contains mapping fields
     * @param bool $commaSplit
     */
    public function setFormFilter(&$formData, $queryDataKey, $apiMapFields = array(), $commaSplit = false)
    {
        //check non empty value
        $keyList = array();

        //we can pass string or array of key to set
        if (is_array($queryDataKey)) {
            $keyList = $queryDataKey;
        } else {
            $keyList[] = $queryDataKey;
        }

        // $_GET data from query_string
        $queryData = $this->queryData;

        //loop through all key to map / assign value
        foreach ($keyList as $key) {
            if (isset($queryData[$key]) && $queryData[$key] != '') {

                //Mapping key to correctkey from apimap array
                if (array_key_exists($key, $apiMapFields)) {
                    $savedkey = 'f' . $apiMapFields[$key];
                } else {
                    $savedkey = $key;
                }

                //Check if need split by comma on value (used for idlist)
                if ($commaSplit) {
                    $queryDataValueTmp = explode(',', $queryData[$key]);
                    $queryDataValue = array();
                    foreach ($queryDataValueTmp as $value) {
                        $value = trim($value);
                        if ($value != '' && in_array($value, $queryDataValue)) {
                            $queryDataValue[] = $value;
                        }
                    }
                } else {
                    //normal case, just assign value
                    $queryDataValue = $queryData[$key];
                }

                //Store value to saved key (key after mapping)
                $formData[$savedkey] = $queryDataValue;
            }
        }
    }


    /**
     * Used to create POST request and immediately timeout to prevent delay
     * @param $url
     * @param array $params
     * @param array $headers
     */
    public function postFireAndForgot($url, $params = array(), $headers = array())
    {
        // create POST string
        $postParams = array();
        foreach ($params as $key => &$val) {
            $postParams[] = $key . '=' . urlencode($val);
        }
        $postString = implode('&', $postParams);

        // get URL segments
        $parts = parse_url($url);

        // workout port and open socket
        $port = isset($parts['port']) ? $parts['port'] : 80;
        $fp = fsockopen($parts['host'], $port, $errno, $errstr, 30);

        // create output string
        $output  = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $output .= "Host: " . $parts['host'] . "\r\n";

        if (is_array($headers)) {
            foreach ($headers as $headername => $headervalue) {
                $output .= "$headername: $headervalue\r\n";
            }
        }

        $output .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $output .= "Content-Length: " . strlen($postString) . "\r\n";
        $output .= "Connection: Close\r\n\r\n";
        $output .= isset($postString) ? $postString : '';

        // send output to $url handle
        fwrite($fp, $output);
        fclose($fp);
    }

    /**
     * This method will insert to queue a message include all information
     * for calling a rest request (webhook)
     *
     * @param array $jsonData, if has 'debug' => 1, message debug will output to supervisor queue log output
     * @param $includeAccessTrustedKey
     * @param string $defaultContentType if jsonData['headers'] do not contain content-type, this request will use this value
     */
    public function queueWebhook($jsonData, $includeAccessTrustedKey = true, $defaultContentType = 'application/json')
    {
        $conf = $this->registry->get('conf');

        if (isset($conf['rabbitmq']) && isset($conf['rabbitmq']['host']) && $conf['rabbitmq']['host'] != '') {

            $exchange = 'teamcrop_webhook_exchange';
            $queue = 'teamcrop_webhook_queue';

            //create connection
            $connection = new AMQPStreamConnection(
                $conf['rabbitmq']['host'],
                $conf['rabbitmq']['port'],
                $conf['rabbitmq']['user'],
                $conf['rabbitmq']['pass'],
                $conf['rabbitmq']['vhost']
            );
            $channel = $connection->channel();

            /*
                name: $queue
                passive: false
                durable: true // the queue will survive server restarts
                exclusive: false // the queue can be accessed in other channels
                auto_delete: false //the queue won't be deleted once the channel is closed.
            */
            $channel->queue_declare($queue, false, true, false, false);

            /*
                name: $exchange
                type: direct
                passive: false
                durable: true // the exchange will survive server restarts
                auto_delete: false //the exchange won't be deleted once the channel is closed.
            */
            $channel->exchange_declare($exchange, 'direct', false, true, false);

            $channel->queue_bind($queue, $exchange);

            //Append accesstrustedkey to header
            if ($includeAccessTrustedKey) {
                if (isset($jsonData['headers'])) {
                    $jsonData['headers']['AccessTrustedKey'] = $conf['trustednetworkaccess']['key'];
                } else {
                    $jsonData['headers'] = array('AccessTrustedKey' => $conf['trustednetworkaccess']['key']);
                }
            }

            //Append content-type to request headers
            if (!isset($jsonData['headers']) || !isset($jsonData['headers']['Content-Type'])) {
                $jsonData['headers']['Content-Type'] = $defaultContentType;
            }

            //Set default method
            if (!isset($jsonData['method']) || $jsonData['method'] == '') {
                $jsonData['method'] = 'POST';
            }

            //Set default rootpath
            //append sdk rooturl if there is no http(s) start
            if (strpos($jsonData['url'], 'http') !== 0) {
                $jsonData['url'] = $this->registry->conf['sdk']['baseurl'] . $jsonData['url'];
            }

            $message = new AMQPMessage(json_encode($jsonData), array(
                'content_type' => 'application/json',
                'delivery_mode' => 2
            ));
            $channel->basic_publish($message, $exchange);

            $channel->close();
            $connection->close();
        }

    }

    /**
     * Add queue message for send notification
     *
     * @param int $companyId
     * @param int $ownerId
     * @param int $creatorId
     * @param string $type
     * @param int $objectId
     * @param int $subObjectId
     * @param string $metaSummary
     * @param string $metaFrom
     * @param string $metaImage
     * @param string $metaUrl
     */
    public function notify(
        $companyId,
        $ownerId,
        $creatorId,
        $type,
        $objectId,
        $subObjectId = 0,
        $metaSummary = '',
        $metaFrom = '',
        $metaImage = '',
        $metaUrl = ''
    ) {
        $this->queueWebhook(array(
            'method' => 'POST',
            'url' => '/v1/notifies',
            'body' => array(
                'company_id' => $companyId,
                'owner_id' => $ownerId,
                'creator_id' => $creatorId,
                'type' => $type,
                'object_id' => $objectId,
                'sub_object_id' => $subObjectId,
                'meta_summary' => $metaSummary,
                'meta_from' => $metaFrom,
                'meta_image' => $metaImage,
                'meta_url' => $metaUrl
            )
        ));
    }

    public function increaseResourceVersion($companyId, $table)
    {
        $this->queueWebhook(array(
            'method' => 'PUT',
            'url' => '/v1/companyresources/increaseversion',
            'body' => array(
                'company_id' => $companyId,
                'table' => $table,
            )
        ));
    }

    public function getIdList($maxItem = 100, $queryName = 'ids')
    {
        $idList = array();

        $item = 0;
        if (isset($this->queryData[$queryName])) {
            $str = trim($this->queryData[$queryName]);
            if ($str != '') {
                $parts = explode(',', $str);
                $item++;
                foreach ($parts as $id) {
                    $id = (int)$id;
                    if ($id > 0 && !in_array($id, $idList) && $item < $maxItem) {
                        $idList[] = $id;
                        $item++;
                    }
                }
            }
        }

        return $idList;
    }

    public function hasCronAccess()
    {
        $pass = false;

        $cronAuthorization = '';
        if (isset($this->queryData['cronpassword']) && $this->queryData['cronpassword'] != '') {
            $cronAuthorization = $this->queryData['cronpassword'];
        }

        if ($this->registry->conf['cron']['password'] == $cronAuthorization && $cronAuthorization != '') {
            $pass = true;
        }

        return $pass;
    }

    /**
     * @return \Elasticsearch\Client
     */
    protected function getSearchEngine()
    {
        if (is_null($this->searchEngineClient)) {
            $hosts = array($this->registry->conf['elasticsearch']['host']);
            $this->searchEngineClient = ClientBuilder::create()->setHosts($hosts)->build();
        }

        return $this->searchEngineClient;
    }
}
