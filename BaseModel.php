<?php

namespace Model;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Litpi\Registry;
use Litpi\Cacher;

class BaseModel extends \Litpi\Model\BaseModel
{
    public static function getInQuestionHolder($values)
    {
        return implode(',', array_fill(0, count($values), '?'));
    }

    public static function callTaskService($uri, $headers, $postData, $debug = false)
    {
        $client = new Client();

        try {
            $client->post($uri, array(
                'headers' => $headers,
                'form_params'    => $postData,
                'debug' => $debug
            ));

        } catch (ClientException $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Luu thong tin vao cache
     * @param string $key
     * @param string $value
     * @return bool
     */
    public static function cacheSet($key, $value)
    {
        $registry = Registry::getInstance();
        $conf = $registry->get('conf');

        $myCacher = new Cacher($key);

        $duration = $conf['cache']['shortdelay'];

        return $myCacher->set($value, $duration);
    }

    /**
     * Used to create POST request and immediately timeout to prevent delay
     * @param $url
     * @param array $params
     * @param array $headers
     */
    public static function postFireAndForgot($url, $params = array(), $headers = array())
    {
        // create POST string
        $postParams = array();
        foreach ($params as $key => &$val) {
            $postParams[] = $key . '=' . urlencode($val);
        }
        $postString = implode('&', $postParams);

        // get URL segments
        $parts = parse_url($url);

        // workout port and open socket
        $port = isset($parts['port']) ? $parts['port'] : 80;
        $fp = fsockopen($parts['host'], $port, $errno, $errstr, 30);

        // create output string
        $output  = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $output .= "Host: " . $parts['host'] . "\r\n";

        if (is_array($headers)) {
            foreach ($headers as $headername => $headervalue) {
                $output .= "$headername: $headervalue\r\n";
            }
        }

        $output .= "Content-Length: " . strlen($postString) . "\r\n";
        $output .= "Connection: Close\r\n\r\n";
        $output .= isset($postString) ? $postString : '';

        // send output to $url handle
        fwrite($fp, $output);
        fclose($fp);
    }
}
