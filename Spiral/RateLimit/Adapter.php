<?php

namespace Spiral\RateLimit;

/**
 * @author Peter Chung <touhonoob@gmail.com>
 * @date May 16, 2015
 */
abstract class Adapter
{
    /**
     * @param $key
     * @param $value
     * @param $ttl
     * @return bool
     */
    abstract public function set($key, $value, $ttl);

    /**
     * @param $key
     * @return mixed
     */
    abstract public function get($key);

    /**
     * @param $key
     * @return bool
     */
    abstract public function exists($key);

    /**
     * @param $key
     * @return bool
     */
    abstract public function del($key);
}